export default {
  install(Vue, options) {
    Vue.mixin({
      methods: {
        CurrencyConverter() {
          return {

            UsdToRub: (usd) => {
              return usd * this.$store.getters['currencies/usdRubRate']
            },
            RubToUsd: (rub) => {
              return rub / this.$store.getters['currencies/usdRubRate']
            },
          }

        }
      }
    })
  }
}
