import Vue from 'vue';
import Vuex from 'vuex';

import products from './products';
import cart from './cart';
import currencies from './currencies';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    products,
    cart,
    currencies
  },
});
