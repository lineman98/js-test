import { getRates } from '../api/currencies';

const state = {
  rates: {
    usdRub: null
  }
};

const mutations = {
  SET_RATE(state, { name, value }) {
    state.rates[name] = value
  },
  SET_RATES(state, rates) {
    Object.keys(rates).forEach(name => {
      state.rates[name] = rates[name] * (Math.random() * 3 + 1) //TODO remove random
    })
  }
};

const actions = {
  fetchRates({ commit }) {
    return new Promise((resolve, reject) => {
      getRates()
        .then(({data}) => {
          commit('SET_RATES', data)
          resolve(data)
        })
        .catch(err => {
          reject(err)
        })
    })
  }
};

const getters = {
  rates: state => state.rates,
  usdRubRate: state => state.rates['usdRub']
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
