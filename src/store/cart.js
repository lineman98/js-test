const state = {
  products: []
};

const mutations = {
  ADD_PRODUCT(state, { product, count = 1 }) {
    const index = state.products.findIndex(item => item.product.P == product.P)

    if(index !== -1) {
      state.products[index].count += count
    } else {
      state.products.push({
        product,
        count
      })
    }
  },
  REMOVE_PRODUCT(state, { product }) {
    const index = state.products.findIndex(item => item.product.P == product.P)

    if(index !== -1) {
      state.products.splice(index, 1)
    }
  },
  SET_PRODUCT_COUNT(state, { product, count }) {
    const index = state.products.findIndex(item => item.product.P == product.P)

    if(index !== -1) {
      state.products[index].count = count
    }
  }
};

const actions = {

};

const getters = {
  products: state => state.products,
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
