import { getProducts, getTranslations } from '../api/products';

const state = {
  products: [],
  translations: [],
};

const mutations = {
  MERGE_PRODUCTS(state, products) {
    products.forEach((item) => {
      if(state.products.findIndex(val => val.P == item.P) == -1) {
        state.products.push(item)
      }
    })
  },
  SET_TRANSLATIONS(state, translations) {
    state.translations = translations
  },
  LINK_TRANSLATIONS(state) {
    state.products.forEach((product) => {
      if(state.translations[product.G] && state.translations[product.G].B[product.T]) {
        product.translation = state.translations[product.G].B[product.T]
      }
    })
  }
};

const actions = {
  async loadProducts({ commit }) {
    return new Promise((resolve, reject) => {
      getProducts()
        .then(({ data }) => {
          commit('MERGE_PRODUCTS', data.Value.Goods)
          commit('LINK_TRANSLATIONS')
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  async loadTranslations({ commit }) {
    return new Promise((resolve, reject) => {
      getTranslations()
        .then(({ data }) => {
          commit('SET_TRANSLATIONS', data)
          commit('LINK_TRANSLATIONS')
          resolve(data)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }
};

const getters = {
  products: state => state.products,
  groupedProducts: (state) => {
    return state.products.reduce((groups, product) => {
      if(groups[product.G] == undefined) {
        groups[product.G] = {
          name: state.translations[product.G] !== undefined ? state.translations[product.G].G : '',
          products: []
        }
      }
      groups[product.G].products.push(product)

      return groups;
    }, {})
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
