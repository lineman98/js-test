import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import CurrencyConverter from './utils/CurrencyConverter';

import './plugins/element-ui.js';

Vue.config.productionTip = false;

Vue.use(CurrencyConverter)

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
