import axios from 'axios';

export async function getProducts(params = {}) {
  return axios.get('/data.json', { params });
}

export async function getTranslations(params = {}) {
  return axios.get('/names.json', { params })
}
