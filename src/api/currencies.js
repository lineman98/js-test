import axios from 'axios';

export async function getRates(params = {}) {
  return axios.get('/rates.json', { params })
}
